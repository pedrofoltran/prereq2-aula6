package br.com.aritmetica.controllers;


import br.com.aritmetica.DTO.EntradaDTO;
import br.com.aritmetica.DTO.RespostaDTO;
import br.com.aritmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaControler {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 números para ser somado.");
        }

        if(!entradaDTO.valorNumeroNatural()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Um dos valores não é número natural.");
        }

        RespostaDTO respostaDTO = matematicaService.soma(entradaDTO);
        return respostaDTO;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 números para ser subtraido");
        }

        if(!entradaDTO.valorNumeroNatural()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Um dos valores não é número natural.");
        }

        RespostaDTO respostaDTO = matematicaService.subtracao(entradaDTO);
        return respostaDTO;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 números para ser multiplicacao");
        }

        if(!entradaDTO.valorNumeroNatural()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Um dos valores não é número natural.");
        }

        RespostaDTO respostaDTO = matematicaService.multiplicacao(entradaDTO);
        return respostaDTO;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 números para ser divisao");
        }

        if (entradaDTO.valorIgualZero()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Divisor igual a zero");
        }

        if (!entradaDTO.multiplicavel()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Valores não divisiveis");
        }

        if(!entradaDTO.valorNumeroNatural()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Um dos valores não é número natural.");
        }

        RespostaDTO respostaDTO = matematicaService.divisao(entradaDTO);
        return respostaDTO;
    }

}
