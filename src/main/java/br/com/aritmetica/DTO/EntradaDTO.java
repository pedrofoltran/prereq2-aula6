package br.com.aritmetica.DTO;

import java.util.List;

public class EntradaDTO {
    private List<Integer> numeros;

    public EntradaDTO() {
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }

    public boolean multiplicavel() {
        int primeiroValor;
        int segundoValor;

        primeiroValor = numeros.get(0);
        segundoValor = numeros.get(1);

        if (primeiroValor % segundoValor == 0)
            return true;
        else
            return false;
    }

    public boolean valorIgualZero() {
        int segundoValor;

        segundoValor = numeros.get(1);

        if (segundoValor == 0)
            return true;
        else
            return false;
    }

    public boolean valorNumeroNatural(){
        for (int valor : numeros){
            if (valor <= 0)
                return false;
        }
        return true;
    }

}
