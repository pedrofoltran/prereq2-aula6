package br.com.aritmetica.services;

import br.com.aritmetica.DTO.EntradaDTO;
import br.com.aritmetica.DTO.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {
    public RespostaDTO soma(EntradaDTO entradaDTO) {
        RespostaDTO resposta = new RespostaDTO();
        int soma = 0;

        for (Integer valor : entradaDTO.getNumeros()) {
            soma += valor;
        }
        resposta.setResultado(soma);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO) {
        RespostaDTO resposta = new RespostaDTO();
        int subtracao = 0;
        boolean primeiroValor = true;

        for (Integer valor : entradaDTO.getNumeros()) {
            if (primeiroValor) {
                subtracao = valor;
                primeiroValor = false;
            } else {
                subtracao -= valor;
            }
        }
        resposta.setResultado(subtracao);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO) {
        RespostaDTO resposta = new RespostaDTO();
        int multiplicacao = 1;

        for (Integer valor : entradaDTO.getNumeros()) {
            multiplicacao *= valor;
        }
        resposta.setResultado(multiplicacao);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO) {
        RespostaDTO resposta = new RespostaDTO();
        int primeiroValor;
        int segundoValor;
        int divisao;

        primeiroValor = entradaDTO.getNumeros().get(0);
        segundoValor = entradaDTO.getNumeros().get(1);

        divisao = primeiroValor / segundoValor;
        resposta.setResultado(divisao);
        return resposta;
    }


}
